from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route("/suma",methods=['POST'])
def sumar():
    content = request.json
    a = content['a']
    b = content['b']
    result = a+b

    return jsonify({"Resultado":result})

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=3000,debug=True)