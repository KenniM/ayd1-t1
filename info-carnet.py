from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route("/carnet",methods=['GET'])
def info():
    return jsonify({"Nombre":"Kenni Roberto Martínez Marroquin", "Carnet":201800457})

@app.route("/test",methods=['GET'])
def test():
    return jsonify({"Result":"OK"})

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=4000,debug=True)